/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package ds2.arida.internal.wow.combatlog.parser.impl;

import javax.inject.Inject;

import org.testng.Assert;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;

import ds2.arida.internal.wow.combatlog.parser.api.UnitFlag;
import ds2.arida.internal.wow.combatlog.parser.api.UnitFlagController;
import ds2.arida.internal.wow.combatlog.parser.api.UnitFlagControllerAffiliation;
import ds2.arida.internal.wow.combatlog.parser.api.UnitFlagParser;
import ds2.arida.internal.wow.combatlog.parser.api.UnitFlagReaction;
import ds2.arida.internal.wow.combatlog.parser.api.UnitFlagType;

/**
 * Testcase for the unitflag parser.
 * 
 * @author dstrauss
 * @version 1.0
 */
@Guice(modules = InjectionPlan.class)
@Test(groups = "unitflag")
public class UnitFlagParserImplTest {
    /**
     * The test object.
     */
    @Inject
    private UnitFlagParser to;
    
    /**
     * Inits the test.
     */
    public UnitFlagParserImplTest() {
        // nothing special to do
    }
    
    /**
     * PVP flag.
     */
    @Test
    public final void pvp() {
        final UnitFlag flag = to.parseUnitFlag(0x0548);
        Assert.assertNotNull(flag);
        Assert.assertEquals(flag.getController(), UnitFlagController.Player);
        Assert.assertEquals(flag.getControllerAffiliation(),
            UnitFlagControllerAffiliation.OUTSIDER);
        Assert.assertEquals(flag.getReaction(), UnitFlagReaction.HOSTILE);
        Assert.assertEquals(flag.getType(), UnitFlagType.Player);
    }
    
    /**
     * Illidan in WoE.
     */
    @Test
    public final void testIllidan() {
        final UnitFlag flag = to.parseUnitFlag(0xa18);
        Assert.assertNotNull(flag);
        Assert.assertEquals(flag.getController(), UnitFlagController.NPC);
        Assert.assertEquals(flag.getControllerAffiliation(),
            UnitFlagControllerAffiliation.OUTSIDER);
        Assert.assertEquals(flag.getReaction(), UnitFlagReaction.FRIENDLY);
        Assert.assertEquals(flag.getType(), UnitFlagType.NPC);
    }
    
    /**
     * Arestonius check.
     */
    @Test
    public final void testAres() {
        final UnitFlag flag = to.parseUnitFlag(0x512);
        Assert.assertNotNull(flag);
        Assert.assertEquals(flag.getController(), UnitFlagController.Player);
        Assert.assertEquals(flag.getControllerAffiliation(),
            UnitFlagControllerAffiliation.PARTY);
        Assert.assertEquals(flag.getReaction(), UnitFlagReaction.FRIENDLY);
        Assert.assertEquals(flag.getType(), UnitFlagType.Player);
    }
    
    /**
     * Sharrath check.
     */
    @Test
    public final void testSharrath() {
        final UnitFlag flag = to.parseUnitFlag(0x511);
        Assert.assertNotNull(flag);
        Assert.assertEquals(flag.getController(), UnitFlagController.Player);
        Assert.assertEquals(flag.getControllerAffiliation(),
            UnitFlagControllerAffiliation.MINE);
        Assert.assertEquals(flag.getReaction(), UnitFlagReaction.FRIENDLY);
        Assert.assertEquals(flag.getType(), UnitFlagType.Player);
    }
    
    /**
     * Katze check.
     */
    @Test
    public final void testKatze() {
        final UnitFlag flag = to.parseUnitFlag(0x1112);
        Assert.assertNotNull(flag);
        Assert.assertEquals(flag.getController(), UnitFlagController.Player);
        Assert.assertEquals(flag.getControllerAffiliation(),
            UnitFlagControllerAffiliation.PARTY);
        Assert.assertEquals(flag.getReaction(), UnitFlagReaction.FRIENDLY);
        Assert.assertEquals(flag.getType(), UnitFlagType.Pet);
    }
}
