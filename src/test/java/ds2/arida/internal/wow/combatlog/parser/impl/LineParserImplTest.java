/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ds2.arida.internal.wow.combatlog.parser.impl;

import javax.inject.Inject;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;

import ds2.arida.internal.wow.combatlog.parser.api.AuraType;
import ds2.arida.internal.wow.combatlog.parser.api.Event;
import ds2.arida.internal.wow.combatlog.parser.api.EventTypeEnum;
import ds2.arida.internal.wow.combatlog.parser.api.LineParserService;
import ds2.arida.internal.wow.combatlog.parser.api.ParserSettings;

/**
 * A test to check the parsing of incoming log lines.
 * 
 * @author dstrauss
 * @version 1.0
 */
@Guice(modules = InjectionPlan.class)
@Test(groups = { "lineparser" })
public class LineParserImplTest {
    /**
     * The service to test.
     */
    @Inject
    private LineParserService to;
    /**
     * The parser settings.
     */
    @Inject
    private ParserSettings setts;
    
    /**
     * Inits the test class.
     */
    public LineParserImplTest() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Actions to perform on class load.
     */
    @BeforeTest(alwaysRun = true)
    public final void onClass() {
        setts.setInitialYear(2011);
    }
    
    /**
     * Simple null test.
     */
    @Test(groups = "null")
    public final void testNull() {
        Assert.assertNull(to.parseLine(null));
    }
    
    /**
     * Spell_Aura_removed test.
     */
    @Test(groups = "spellAuraRemoved")
    public final void parseSpellAuraRemoved() {
        final String line =
            "12/14 21:07:00.845  SPELL_AURA_REMOVED,0x0480000001513886,\"Grimbaldi-Nazjatar\",0x512,0x0,0x0480000001513886,\"Grimbaldi-Nazjatar\",0x512,0x0,82692,\"Feuer konzentrieren\",0x1,BUFF";
        final Event res = to.parseLine(line);
        Assert.assertNotNull(res);
        Assert.assertEquals(res.getEventType(),
            EventTypeEnum.SPELL_AURA_REMOVED);
        Assert.assertNotNull(res.getOriginator());
        Assert.assertNotNull(res.getTarget());
        Assert.assertNotNull(res.getAction());
        Assert.assertEquals(res.getAction().getName(), "Feuer konzentrieren");
        Assert.assertEquals(res.getAction().getSpellId(), 82692);
        Assert.assertEquals(res.getAction().getAuraType(), AuraType.BUFF);
    }
    
    /**
     * Swing_damage test.
     */
    @Test(groups = "swingDamage")
    public final void parseSwingDamage() {
        final String line =
            "12/14 21:07:45.257  SWING_DAMAGE,0x0480000000762C32,\"Arestonius\",0x512,0x0,0xF130D96800050A17,\"Schreckenslordverteidiger\",0xa48,0x0,28941,-1,1,0,0,0,1,nil,nil";
        final Event res = to.parseLine(line);
        Assert.assertNotNull(res);
    }
    
}
