/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ds2.arida.internal.wow.combatlog.parser.impl;

import javax.inject.Inject;

import org.testng.Assert;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;

import ds2.arida.internal.wow.combatlog.parser.api.GuidParser;
import ds2.arida.internal.wow.combatlog.parser.api.Unit;
import ds2.arida.internal.wow.combatlog.parser.api.UnitType;
import ds2.core.api.svc.HexCodec;

/**
 * The guid parser tests.
 * 
 * @author dstrauss
 * @version 1.0
 */
@Guice(modules = InjectionPlan.class)
@Test(groups = { "guid", "boss", "pet", "vehicle", "player" })
public class GuidParserTest {
    /**
     * The parser to test.
     */
    @Inject
    private GuidParser to;
    /**
     * The hex codec.
     */
    @Inject
    private HexCodec hx;
    
    /**
     * Inits the test.
     */
    public GuidParserTest() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Simple hex checks.
     */
    @Test
    public final void hexCheck() {
        Assert.assertEquals(0xd8cc, 55500);
        Assert.assertEquals(0xd968, 55656);
        Assert.assertEquals(0xad24, 44324);
    }
    
    /**
     * Simple hex check.
     */
    @Test
    public final void testHex() {
        final byte[] b = hx.decode("4d2b".toCharArray());
        Assert.assertNotNull(b);
        Assert.assertTrue(b.length == 2);
        Assert.assertEquals(b[0], 0x4d);
        Assert.assertEquals(b[1], 0x2b);
        System.out.println("b0=" + b[0]);
        System.out.println("b1=" + b[1]);
    }
    
    /**
     * Test Klauenkoenig Ikiss.
     */
    @Test(groups = { "boss" })
    public final void testInstanceBoss1() {
        final Unit u = to.parse("0xF13048290000AD24");
        Assert.assertNotNull(u);
        Assert.assertEquals(u.getType(), UnitType.NPC);
        Assert.assertEquals(u.getUnitId(), 0x4829);
        Assert.assertEquals(u.getSpawnId(), 0xad24);
    }
    
    /**
     * Permpet test.
     */
    @Test(groups = { "pet" })
    public final void testKatze() {
        final Unit u = to.parse("0xF1409DAD84000180");
        Assert.assertNotNull(u);
        Assert.assertEquals(u.getType(), UnitType.PERMPET);
        Assert.assertEquals(u.getUnitId(), 10333572);
    }
    
    /**
     * <p>
     * Pre-defined sample from wowwiki. I had to re-edit it as the guid is not
     * compliant compared to Illidan test and Schreckenslord test (real
     * combatlog tests).
     * </p>
     * <p>
     * Explanation: <a
     * href="http://www.wowwiki.com/API_UnitGUID#4.0_changes_to_NPC_ID">4.0
     * changes</a>
     * </p>
     * 
     */
    @Test(groups = "npc")
    public final void testMoarq() {
        final Unit u = to.parse("F5304D2B00008852");
        Assert.assertNotNull(u);
        Assert.assertEquals(u.getType(), UnitType.NPC);
        Assert.assertEquals(u.getUnitId(), 0x4d2b);
        Assert.assertEquals(u.getSpawnId(), 0x8852);
    }
    
    /**
     * Simple null check.
     */
    @Test(groups = "null")
    public final void testNull() {
        Assert.assertNull(to.parse(null));
    }
    
    /**
     * Player detection.
     */
    @Test(groups = "player")
    public final void testPlayerAkeea() {
        final Unit u = to.parse("048000000330D0E4");
        Assert.assertNotNull(u);
        Assert.assertEquals(u.getType(), UnitType.PLAYER);
        Assert.assertEquals(u.getUnitId(), 0x330d0e4);
    }
    
    /**
     * Player detection.
     */
    @Test(groups = "player")
    public final void testPlayerSharrath() {
        final Unit u = to.parse("0480000000643559");
        Assert.assertNotNull(u);
        Assert.assertEquals(u.getType(), UnitType.PLAYER);
        Assert.assertEquals(u.getUnitId(), 0x643559);
    }
    
    /**
     * Test for some Well of Eternity trash.
     */
    @Test(groups = "npc")
    public final void testSchreckenslordverteidiger() {
        final Unit u = to.parse("F130D96800050A17");
        Assert.assertNotNull(u);
        Assert.assertEquals(u.getType(), UnitType.NPC);
        Assert.assertEquals(u.getUnitId(), 55656);
        Assert.assertEquals(u.getSpawnId(), 330263);
    }
    
    /**
     * Test Illidan Sturmgrimm, Well of Eternity.
     */
    @Test(groups = "vehicle")
    public final void testVehicleIllidanWoE() {
        final Unit u = to.parse("F150D8CC00051585");
        Assert.assertNotNull(u);
        Assert.assertEquals(u.getType(), UnitType.VEHICLE);
        Assert.assertEquals(u.getUnitId(), 55500);
        Assert.assertEquals(u.getSpawnId(), 0x51585);
    }
}
