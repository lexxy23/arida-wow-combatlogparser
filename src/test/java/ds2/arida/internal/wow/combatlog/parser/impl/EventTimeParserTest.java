/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ds2.arida.internal.wow.combatlog.parser.impl;

import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;

import ds2.arida.internal.wow.combatlog.parser.api.EventTimeParser;
import ds2.arida.internal.wow.combatlog.parser.api.ParserSettings;

/**
 * A test for the event time parser.
 * 
 * @author dstrauss
 * @version 1.0
 */
@Guice(modules = InjectionPlan.class)
@Test(groups = "timeparser")
public class EventTimeParserTest {
    /**
     * The time parser.
     */
    @Inject
    private EventTimeParser to;
    /**
     * The parser settings.
     */
    @Inject
    private ParserSettings setts;
    
    /**
     * Inits the test.
     */
    public EventTimeParserTest() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Actions to perform on test start.
     */
    @BeforeTest
    public final void onTest() {
        setts.setInitialYear(2011);
    }
    
    /**
     * Simple null check.
     */
    @Test(groups = "null")
    public void testNull() {
        Assert.assertNull(to.parseTimeFromLogline(null));
    }
    
    /**
     * Parser test 1.
     */
    @Test
    public final void testParse1() {
        final Date d = to.parseTimeFromLogline("12/14 21:07:00.845");
        Assert.assertNotNull(d);
        Assert.assertEquals(d, createDate(2011, 12, 14, 21, 7, 0, 845));
    }
    
    /**
     * Creates a fixed date.
     * 
     * @param year
     *            the year
     * @param month
     *            the month (1=jan, 2=feb etc.)
     * @param day
     *            the day of month
     * @param hour
     *            the hour (24-hour-clock)
     * @param minute
     *            the minute
     * @param seconds
     *            the seconds
     * @param millis
     *            the milliseconds
     * @return the created date
     */
    private Date createDate(int year, int month, int day, int hour, int minute,
        int seconds, int millis) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month - 1);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, seconds);
        cal.set(Calendar.MILLISECOND, millis);
        return cal.getTime();
    }
    
}
