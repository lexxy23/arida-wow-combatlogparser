/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ds2.arida.internal.wow.combatlog.parser.impl;

import com.google.inject.AbstractModule;

import ds2.arida.internal.wow.combatlog.parser.api.ActionParser;
import ds2.arida.internal.wow.combatlog.parser.api.EventTimeParser;
import ds2.arida.internal.wow.combatlog.parser.api.GuidParser;
import ds2.arida.internal.wow.combatlog.parser.api.LineParserService;
import ds2.arida.internal.wow.combatlog.parser.api.ParserSettings;
import ds2.arida.internal.wow.combatlog.parser.api.SimpleTypesParser;
import ds2.arida.internal.wow.combatlog.parser.api.UnitFlagParser;
import ds2.core.api.BitSupport;
import ds2.core.api.svc.HexCodec;
import ds2.core.base.impl.BitSupportImpl;
import ds2.core.base.impl.HexKonverter;

/**
 * The injection plan.
 * 
 * @author dstrauss
 * @version 1.0
 */
public class InjectionPlan extends AbstractModule {
    
    /**
     * Inits the plan.
     */
    public InjectionPlan() {
        // nothing special to do
    }
    
    @Override
    protected final void configure() {
        bind(LineParserService.class).to(LineParserServiceImpl.class);
        bind(EventTimeParser.class).to(EventTimeParserImpl.class);
        bind(ParserSettings.class);
        bind(GuidParser.class).to(GuidParserImpl.class);
        bind(HexCodec.class).to(HexKonverter.class);
        bind(BitSupport.class).to(BitSupportImpl.class);
        bind(SimpleTypesParser.class).to(SimpleTypesParserImpl.class);
        bind(ActionParser.class).to(ActionParserImpl.class);
        bind(UnitFlagParser.class).to(UnitFlagParserImpl.class);
    }
    
}
