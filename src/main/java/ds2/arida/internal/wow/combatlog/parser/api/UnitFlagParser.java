/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package ds2.arida.internal.wow.combatlog.parser.api;

/**
 * The parser contract for unit flag parsing.
 * 
 * @author dstrauss
 * @version 1.0
 */
public interface UnitFlagParser {
    /**
     * Parses the given unit flag.
     * 
     * @param flag
     *            the flag value
     * @return the detected unit flag
     */
    UnitFlag parseUnitFlag(int flag);
    
    /**
     * Parses the given unit flag from a given hex string.
     * 
     * @param string
     *            the hex string, starting with 0x and being a hexadecimal value
     * @return the detected unit flags, or null if not found
     */
    UnitFlag parseUnitFlagFromHexString(String string);
}
