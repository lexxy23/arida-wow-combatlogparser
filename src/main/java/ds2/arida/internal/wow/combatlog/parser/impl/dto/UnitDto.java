/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ds2.arida.internal.wow.combatlog.parser.impl.dto;

import ds2.arida.internal.wow.combatlog.parser.api.Unit;
import ds2.arida.internal.wow.combatlog.parser.api.UnitFlag;
import ds2.arida.internal.wow.combatlog.parser.api.UnitType;

/**
 * The dto for a unit.
 * 
 * @author dstrauss
 * @version 1.0
 */
public class UnitDto implements Unit {
    
    /**
     * The svuid.
     */
    private static final long serialVersionUID = 5747834306526323285L;
    /**
     * The name of the unit.
     */
    private String name;
    /**
     * The unit type.
     */
    private UnitType type;
    /**
     * The possible bg identifier.
     */
    private Integer bgIdentifier;
    /**
     * The unit id.
     */
    private long unitId;
    /**
     * The spawn id.
     */
    private long spawnId;
    /**
     * The guid that was used to parse this unit.
     */
    private long guid;
    /**
     * The unit flags.
     */
    private UnitFlag flags;
    
    /**
     * Inits the dto.
     */
    public UnitDto() {
        // nothing special to do
    }
    
    @Override
    public final String getName() {
        return name;
    }
    
    @Override
    public final UnitType getType() {
        return type;
    }
    
    @Override
    public final Integer getBgIdentifier() {
        return bgIdentifier;
    }
    
    @Override
    public final long getUnitId() {
        return unitId;
    }
    
    @Override
    public final long getSpawnId() {
        return spawnId;
    }
    
    @Override
    public final void setName(final String n) {
        this.name = n;
    }
    
    /**
     * Sets the unit type.
     * 
     * @param t
     *            the unit type
     */
    public final void setType(final UnitType t) {
        this.type = t;
    }
    
    /**
     * Sets the instance id, or battleground id.
     * 
     * @param bId
     *            the bg identifier
     */
    public final void setBgIdentifier(final Integer bId) {
        this.bgIdentifier = bId;
    }
    
    /**
     * Sets the unit id.
     * 
     * @param uId
     *            the unit id
     */
    public final void setUnitId(final long uId) {
        this.unitId = uId;
    }
    
    @Override
    public final long getGuid() {
        return guid;
    }
    
    /**
     * Sets the spawn id.
     * 
     * @param sId
     *            the spawnId to set
     */
    public final void setSpawnId(final long sId) {
        this.spawnId = sId;
    }
    
    /**
     * Sets the guid.
     * 
     * @param gId
     *            the guid to set
     */
    public final void setGuid(final long gId) {
        this.guid = gId;
    }
    
    @Override
    public final String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("UnitDto [name=");
        builder.append(name);
        builder.append(", type=");
        builder.append(type);
        builder.append(", bgIdentifier=");
        builder.append(bgIdentifier);
        builder.append(", unitId=");
        builder.append(unitId);
        builder.append(", spawnId=");
        builder.append(spawnId);
        builder.append(", guid=");
        builder.append(guid);
        builder.append(", flags=");
        builder.append(flags);
        builder.append("]");
        return builder.toString();
    }
    
    @Override
    public final UnitFlag getFlags() {
        return flags;
    }
    
    /**
     * Sets the flags.
     * 
     * @param f
     *            the flags to set
     */
    public final void setFlags(final UnitFlag f) {
        this.flags = f;
    }
    
}
