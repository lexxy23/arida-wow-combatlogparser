/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ds2.arida.internal.wow.combatlog.parser.impl;

import ds2.arida.internal.wow.combatlog.parser.api.SimpleTypesParser;

/**
 * The types parser.
 * 
 * @author dstrauss
 * @version 1.0
 */
public class SimpleTypesParserImpl implements SimpleTypesParser {
    
    /**
     * Inits the parser.
     */
    public SimpleTypesParserImpl() {
        // nothing special to do.
    }
    
    @Override
    public final int toInt(final String s, final int def) {
        int rc = def;
        if (s == null || "nil".equalsIgnoreCase(s)) {
            return def;
        }
        try {
            rc = Integer.parseInt(s);
        } catch (final NumberFormatException e) {
            // silently ignored
        }
        return rc;
    }
    
    @Override
    public final String parseString(final String string) {
        final StringBuilder sb = new StringBuilder(string);
        if (sb.charAt(0) == '\"') {
            sb.deleteCharAt(0);
        }
        if (sb.charAt(sb.length() - 1) == '\"') {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }
    
    @Override
    public final boolean toBoolean(final String string, final boolean b) {
        if ("1".equalsIgnoreCase(string)) {
            return true;
        }
        return false;
    }
    
}
