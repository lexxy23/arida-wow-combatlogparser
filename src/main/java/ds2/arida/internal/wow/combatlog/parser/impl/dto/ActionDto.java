/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ds2.arida.internal.wow.combatlog.parser.impl.dto;

import ds2.arida.internal.wow.combatlog.parser.api.Action;
import ds2.arida.internal.wow.combatlog.parser.api.AuraType;

/**
 * An action dto.
 * 
 * @author dstrauss
 * @version 1.0
 */
public class ActionDto implements Action {
    /**
     * The name of the action.
     */
    private String name;
    /**
     * The spell school id.
     */
    private int spellSchoolId;
    /**
     * The spell id.
     */
    private int spellId;
    /**
     * The aura type.
     */
    private AuraType auraType;
    /**
     * The amount.
     */
    private int amount;
    /**
     * The overkill amount.
     */
    private int overKillAmount;
    /**
     * The resisted amount.
     */
    private int resistedAmount;
    /**
     * The blocked amount.
     */
    private int blockedAmount;
    /**
     * The absorbed amount.
     */
    private int absorbedAmount;
    /**
     * The critical flag.
     */
    private boolean critical;
    /**
     * The glancing flag.
     */
    private boolean glancing;
    /**
     * The crushing flag.
     */
    private boolean crushing;
    
    /**
     * Inits the dto.
     */
    public ActionDto() {
        // nothing special to do
    }
    
    @Override
    public final String getName() {
        return name;
    }
    
    /**
     * Sets the name of the action.
     * 
     * @param n
     *            the name to set
     */
    public final void setName(final String n) {
        this.name = n;
    }
    
    @Override
    public final int getOverKillAmount() {
        return overKillAmount;
    }
    
    @Override
    public final int getResistedAmount() {
        return resistedAmount;
    }
    
    @Override
    public final int getBlockedAmount() {
        return blockedAmount;
    }
    
    @Override
    public final int getAbsorbedAmount() {
        return absorbedAmount;
    }
    
    @Override
    public final boolean isCritical() {
        return critical;
    }
    
    @Override
    public final boolean isGlancing() {
        return glancing;
    }
    
    @Override
    public final boolean isCrushing() {
        return crushing;
    }
    
    @Override
    public final int getAmount() {
        return amount;
    }
    
    @Override
    public final AuraType getAuraType() {
        return auraType;
    }
    
    @Override
    public final int getSpellId() {
        return spellId;
    }
    
    @Override
    public final String getSpellName() {
        return name;
    }
    
    @Override
    public final int getSpellSchoolId() {
        return spellSchoolId;
    }
    
    /**
     * Sets the spell school id.
     * 
     * @param sId
     *            the spellSchoolId to set
     */
    public final void setSpellSchoolId(final int sId) {
        this.spellSchoolId = sId;
    }
    
    /**
     * Sets the spell id.
     * 
     * @param sId
     *            the spellId to set
     */
    public final void setSpellId(final int sId) {
        this.spellId = sId;
    }
    
    /**
     * Sets the aura type.
     * 
     * @param aType
     *            the auraType to set
     */
    public final void setAuraType(final AuraType aType) {
        this.auraType = aType;
    }
    
    @Override
    public final String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("ActionDto [name=");
        builder.append(name);
        builder.append(", spellSchoolId=");
        builder.append(spellSchoolId);
        builder.append(", spellId=");
        builder.append(spellId);
        builder.append(", auraType=");
        builder.append(auraType);
        builder.append(", amount=");
        builder.append(amount);
        builder.append(", overKillAmount=");
        builder.append(overKillAmount);
        builder.append(", resistedAmount=");
        builder.append(resistedAmount);
        builder.append(", blockedAmount=");
        builder.append(blockedAmount);
        builder.append(", absorbedAmount=");
        builder.append(absorbedAmount);
        builder.append(", critical=");
        builder.append(critical);
        builder.append(", glancing=");
        builder.append(glancing);
        builder.append(", crushing=");
        builder.append(crushing);
        builder.append("]");
        return builder.toString();
    }
    
    /**
     * Sets the amount. Whether healing, damage etc.
     * 
     * @param a
     *            the amount to set
     */
    public final void setAmount(final int a) {
        this.amount = a;
    }
    
    /**
     * Sets the overkill amount.
     * 
     * @param oA
     *            the overKillAmount to set
     */
    public final void setOverKillAmount(final int oA) {
        this.overKillAmount = oA;
    }
    
    /**
     * Sets the resisted amount.
     * 
     * @param rA
     *            the resistedAmount to set
     */
    public final void setResistedAmount(final int rA) {
        this.resistedAmount = rA;
    }
    
    /**
     * Sets the blocked amount.
     * 
     * @param bA
     *            the blockedAmount to set
     */
    public final void setBlockedAmount(final int bA) {
        this.blockedAmount = bA;
    }
    
    /**
     * Sets the absorbed amount.
     * 
     * @param aA
     *            the absorbedAmount to set
     */
    public final void setAbsorbedAmount(final int aA) {
        this.absorbedAmount = aA;
    }
    
    /**
     * Sets the critical flag.
     * 
     * @param c
     *            the critical to set
     */
    public final void setCritical(final boolean c) {
        this.critical = c;
    }
    
    /**
     * Sets the glancing amount.
     * 
     * @param g
     *            the glancing to set
     */
    public final void setGlancing(final boolean g) {
        this.glancing = g;
    }
    
    /**
     * Sets the crushing flag.
     * 
     * @param c
     *            the crushing to set
     */
    public final void setCrushing(final boolean c) {
        this.crushing = c;
    }
    
}
