/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ds2.arida.internal.wow.combatlog.parser.api;

/**
 * An event type.
 * 
 * @author dstrauss
 * @version 1.0
 */
public interface EventType {
    /**
     * Returns the possible event prefix.
     * 
     * @return the event prefix, or null if not set.
     */
    EventPrefix getPrefix();
    
    /**
     * Returns the possible event suffix.
     * 
     * @return the event suffix, or null if not set.
     */
    EventSuffix getSuffix();
}
