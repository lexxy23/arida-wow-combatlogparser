/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package ds2.arida.internal.wow.combatlog.parser.api;

import java.io.Serializable;
import java.util.Date;

/**
 * A single event, as occurring in a single combat line log.
 * 
 * @author dstrauss
 * @version 1.0
 */
public interface Event extends Comparable<Event>, Serializable {
    /**
     * Returns the time of the event.
     * 
     * @return the time
     */
    Date getTimestamp();
    
    /**
     * Returns the originator of the event. Usually the one who performed the
     * action.
     * 
     * @return the originator
     */
    Unit getOriginator();
    
    /**
     * REturns the target of the action.
     * 
     * @return the target, if available
     */
    Unit getTarget();
    
    /**
     * Returns the action that was performed by the originator.
     * 
     * @return the action
     */
    Action getAction();
    
    /**
     * Returns the event type.
     * 
     * @return the type of the event.
     */
    EventTypeEnum getEventType();
}
