/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package ds2.arida.internal.wow.combatlog.parser.api;

/**
 * The known power types.
 * 
 * @author dstrauss
 * @version 1.0
 */
public enum PowerType {
    /**
     * Health.
     */
    HEALTH,
    /**
     * Mana.
     */
    MANA,
    /**
     * Rage.
     */
    RAGE,
    /**
     * Focus.
     */
    FOCUS,
    /**
     * Energy.
     */
    ENERGY,
    /**
     * Hunter Pet happiness.
     */
    PET_HAPPINESS,
    /**
     * Death knight Runes.
     */
    RUNES,
    /**
     * Runic power.
     */
    RUNIC_POWER,
    /**
     * Soul shard.
     */
    SOUL_SHARD,
    /**
     * Druid eclipse energy.
     */
    ECLIPSE_ENERGY,
    /**
     * Paladin holy power.
     */
    HOLY_POWER;
}
