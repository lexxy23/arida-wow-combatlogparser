/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package ds2.arida.internal.wow.combatlog.parser.api;

/**
 * All known handleable event types.
 * 
 * @author dstrauss
 * @version 1.0
 */
public enum EventTypeEnum {
    /**
     * Spell aura removed.
     */
    SPELL_AURA_REMOVED(EventPrefix.SPELL, EventSuffix.AURA_REMOVED),
    /**
     * A swing melee damage.
     */
    SWING_DAMAGE(EventPrefix.SWING, EventSuffix.DAMAGE);
    ;
    /**
     * The event prefix.
     */
    private EventPrefix prefix;
    /**
     * The event suffix.
     */
    private EventSuffix suffix;
    
    /**
     * Inits the enum value.
     * 
     * @param p
     *            the prefix
     * @param s
     *            the suffix
     */
    private EventTypeEnum(final EventPrefix p, final EventSuffix s) {
        prefix = p;
        suffix = s;
    }
    
    /**
     * Returns the known prefix.
     * 
     * @return the prefix, or null if not set.
     */
    public EventPrefix getPrefix() {
        return prefix;
    }
    
    /**
     * Returns the event suffix.
     * 
     * @return the suffix, or null if not set.
     */
    public EventSuffix getSuffix() {
        return suffix;
    }
}
