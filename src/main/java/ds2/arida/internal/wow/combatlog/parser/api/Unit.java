/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package ds2.arida.internal.wow.combatlog.parser.api;

import java.io.Serializable;

/**
 * The unit guid.
 * 
 * @author dstrauss
 * @version 1.0
 */
public interface Unit extends Serializable {
    /**
     * Returns the unit name.
     * 
     * @return the unit name. May return null if not set.
     */
    String getName();
    
    /**
     * Returns the type of the unit.
     * 
     * @return the unit type
     */
    UnitType getType();
    
    /**
     * Returns the battleground identifier. It may also be some kind of instance
     * id.
     * 
     * @return the bg identifier. May return null.
     */
    Integer getBgIdentifier();
    
    /**
     * Returns the unit identifier. For NPCs, this is the unit id. For players,
     * this is the unique player id. For pets, this is the pet creation id from
     * the server. If it's a world object, this is the world object id. For
     * vehicles, this is the vehicle id.
     * 
     * @return the unit identifier
     */
    long getUnitId();
    
    /**
     * Returns the spawn id. Usually only applicable to NPCs.
     * 
     * @return the spawn.
     */
    long getSpawnId();
    
    /**
     * Returns the guid that was used to parse this unit.
     * 
     * @return the guid
     */
    long getGuid();
    
    /**
     * Sets the unit name.
     * 
     * @param u
     *            the unit name
     */
    void setName(String u);
    
    /**
     * Returns the initial unit flags for this unit.
     * 
     * @return the unit flags
     */
    UnitFlag getFlags();
    
    /**
     * Sets the unit flags.
     * 
     * @param f
     *            the unit flag
     */
    void setFlags(UnitFlag f);
}
