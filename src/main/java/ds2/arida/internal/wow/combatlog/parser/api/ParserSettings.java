/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ds2.arida.internal.wow.combatlog.parser.api;

import java.io.File;
import java.io.Serializable;

import javax.inject.Singleton;

/**
 * The settings for the combatlog parser.
 * 
 * @author dstrauss
 * @version 1.0
 */
@Singleton
public class ParserSettings implements Serializable {
    /**
     * The svuid.
     */
    private static final long serialVersionUID = 4223994343335590766L;
    /**
     * The location of the WoW game.
     */
    private File wowFolder;
    /**
     * The year to use when checking the event dates.
     */
    private int initialYear;
    
    /**
     * Inits the dto.
     */
    public ParserSettings() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Returns the location of the WoW game.
     * 
     * @return the wowFolder
     */
    public synchronized File getWowFolder() {
        return wowFolder;
    }
    
    /**
     * Returns the year for events.
     * 
     * @return the year
     */
    public synchronized int getInitialYear() {
        return initialYear;
    }
    
    /**
     * Sets the location of the WoW game.
     * 
     * @param wowFolder
     *            the wowFolder to set
     */
    public synchronized void setWowFolder(final File wowFolder) {
        this.wowFolder = wowFolder;
    }
    
    /**
     * Sets the year.
     * 
     * @param year
     *            the year to set
     */
    public synchronized void setInitialYear(final int year) {
        this.initialYear = year;
    }
    
}
