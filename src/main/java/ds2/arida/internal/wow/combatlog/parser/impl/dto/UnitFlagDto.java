/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package ds2.arida.internal.wow.combatlog.parser.impl.dto;

import ds2.arida.internal.wow.combatlog.parser.api.UnitFlag;
import ds2.arida.internal.wow.combatlog.parser.api.UnitFlagController;
import ds2.arida.internal.wow.combatlog.parser.api.UnitFlagControllerAffiliation;
import ds2.arida.internal.wow.combatlog.parser.api.UnitFlagReaction;
import ds2.arida.internal.wow.combatlog.parser.api.UnitFlagType;

/**
 * The unitflag dto.
 * 
 * @author dstrauss
 * @version 1.0
 */
public class UnitFlagDto implements UnitFlag {
    /**
     * The type.
     */
    private UnitFlagType type;
    /**
     * The controller.
     */
    private UnitFlagController controller;
    /**
     * The reaction.
     */
    private UnitFlagReaction reaction;
    /**
     * The affiliation.
     */
    private UnitFlagControllerAffiliation controllerAffiliation;
    
    /**
     * Inits the dto.
     */
    public UnitFlagDto() {
        // nothing special to do
    }
    
    @Override
    public final UnitFlagType getType() {
        return type;
    }
    
    @Override
    public final UnitFlagController getController() {
        return controller;
    }
    
    @Override
    public final UnitFlagReaction getReaction() {
        return reaction;
    }
    
    @Override
    public final UnitFlagControllerAffiliation getControllerAffiliation() {
        return controllerAffiliation;
    }
    
    /**
     * Sets the type.
     * 
     * @param t
     *            the type to set
     */
    public final void setType(final UnitFlagType t) {
        this.type = t;
    }
    
    /**
     * Sets the controller.
     * 
     * @param c
     *            the controller to set
     */
    public final void setController(final UnitFlagController c) {
        this.controller = c;
    }
    
    /**
     * Sets the reaction.
     * 
     * @param r
     *            the reaction to set
     */
    public final void setReaction(final UnitFlagReaction r) {
        this.reaction = r;
    }
    
    /**
     * Sets the affiliation.
     * 
     * @param cA
     *            the controllerAffiliation to set
     */
    public final void setControllerAffiliation(
        final UnitFlagControllerAffiliation cA) {
        this.controllerAffiliation = cA;
    }
    
    @Override
    public final String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("UnitFlagDto [type=");
        builder.append(type);
        builder.append(", controller=");
        builder.append(controller);
        builder.append(", reaction=");
        builder.append(reaction);
        builder.append(", controllerAffiliation=");
        builder.append(controllerAffiliation);
        builder.append("]");
        return builder.toString();
    }
    
}
