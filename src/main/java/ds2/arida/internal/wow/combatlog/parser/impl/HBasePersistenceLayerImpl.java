/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package ds2.arida.internal.wow.combatlog.parser.impl;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Singleton;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;

import ds2.arida.internal.wow.combatlog.parser.api.PersistenceLayer;

/**
 * The hbase persistence impl.
 * 
 * @author dstrauss
 * @version 1.0
 */
@Singleton
@ApplicationScoped
public class HBasePersistenceLayerImpl implements PersistenceLayer {
    /**
     * The hbase config.
     */
    private Configuration hbaseConfig;
    
    /**
     * INits the impl.
     */
    public HBasePersistenceLayerImpl() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Some actions to perform on startup.
     * 
     * @throws IOException
     *             if an error occured
     */
    @PostConstruct
    public final void onInit() throws IOException {
        hbaseConfig = HBaseConfiguration.create();
        hbaseConfig.set("hbase.zookeeper.quorum", "localhost");
        HBaseAdmin admin = new HBaseAdmin(hbaseConfig);
        String table = "myTable";
        
        HTable npcTab = new HTable(hbaseConfig, "npc");
        HTable logEntriesTab = new HTable(hbaseConfig, "logs");
        
        admin.disableTable(table);
        admin.close();
        logEntriesTab.close();
        npcTab.close();
    }
    
    /*
     * (non-Javadoc)
     * @see ds2.arida.internal.wow.combatlog.parser.api.PersistenceLayer#store()
     */
    @Override
    public void store() {
        // TODO Auto-generated method stub
        
    }
    
}
