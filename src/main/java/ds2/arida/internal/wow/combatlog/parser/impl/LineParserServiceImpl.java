/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ds2.arida.internal.wow.combatlog.parser.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ds2.arida.internal.wow.combatlog.parser.api.Action;
import ds2.arida.internal.wow.combatlog.parser.api.ActionParser;
import ds2.arida.internal.wow.combatlog.parser.api.Event;
import ds2.arida.internal.wow.combatlog.parser.api.EventTimeParser;
import ds2.arida.internal.wow.combatlog.parser.api.EventTypeEnum;
import ds2.arida.internal.wow.combatlog.parser.api.GuidParser;
import ds2.arida.internal.wow.combatlog.parser.api.LineParserService;
import ds2.arida.internal.wow.combatlog.parser.api.SimpleTypesParser;
import ds2.arida.internal.wow.combatlog.parser.api.Unit;
import ds2.arida.internal.wow.combatlog.parser.api.UnitFlagParser;
import ds2.arida.internal.wow.combatlog.parser.impl.dto.CombatLogLine;

/**
 * The initial version of the line parser.
 * 
 * @author dstrauss
 * @version 1.0
 */
public class LineParserServiceImpl implements LineParserService {
    
    /**
     * A logger.
     */
    private static final Logger LOG = LoggerFactory
        .getLogger(LineParserServiceImpl.class);
    /**
     * The time parser.
     */
    @Inject
    private EventTimeParser time;
    /**
     * The action parser.
     */
    @Inject
    private ActionParser actionSvc;
    /**
     * The guid parser.
     */
    @Inject
    private GuidParser guid;
    /**
     * Simple types parser.
     */
    @Inject
    private SimpleTypesParser types;
    /**
     * The unitflag parser.
     */
    @Inject
    private UnitFlagParser ufparser;
    
    /**
     * Inits the impl.
     */
    public LineParserServiceImpl() {
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public final Event parseLine(final String logLine) {
        LOG.debug("Starting with line: {}", logLine);
        if (logLine == null || logLine.length() <= 0) {
            LOG.debug("No logline given to parse!");
            return null;
        }
        final Date eventTime = time.parseTimeFromLogline(logLine);
        if (eventTime == null) {
            LOG.debug("No eventTime could be parsed. Ignoring further checks.");
            return null;
        }
        final List<String> tokens = getLineTokens(logLine.substring(20));
        final CombatLogLine rc = new CombatLogLine();
        rc.setTimestamp(eventTime);
        final String eventNameId = tokens.get(0);
        LOG.debug("found event name: {}", eventNameId);
        // based on the eventNameId -> choose way to check the given tokens to
        // fill dto
        final EventTypeEnum eventType = getEventTypeByName(eventNameId);
        Unit originator = null;
        Unit target = null;
        Action action = null;
        if (eventType != null) {
            rc.setEventType(eventType);
            originator = guid.parse(tokens.get(1));
            originator.setName(types.parseString(tokens.get(2)));
            originator.setFlags(ufparser.parseUnitFlagFromHexString(tokens
                .get(3)));
            target = guid.parse(tokens.get(5));
            target.setName(types.parseString(tokens.get(6)));
            target.setFlags(ufparser.parseUnitFlagFromHexString(tokens.get(7)));
            action =
                actionSvc.parseAction(eventType,
                    tokens.subList(9, tokens.size()));
        }
        rc.setAction(action);
        rc.setOriginator(originator);
        rc.setTarget(target);
        LOG.debug("result is {}", rc);
        return rc;
    }
    
    /**
     * Returns the event type with the given name.
     * 
     * @param eventNameId
     *            the event type name
     * @return the event type, or null if not found
     */
    private EventTypeEnum getEventTypeByName(final String eventNameId) {
        try {
            final EventTypeEnum rc = EventTypeEnum.valueOf(eventNameId);
            return rc;
        } catch (final IllegalArgumentException e) {
            LOG.warn("Unknown event type: {}", eventNameId);
        }
        return null;
    }
    
    /**
     * Creates a list of all known tokens.
     * 
     * @param l
     *            the combat log line, starting with the event name.
     * @return the list of tokens from the given combatlog line
     */
    private List<String> getLineTokens(final String l) {
        LOG.debug("Parsing line: {}", l);
        final StringTokenizer st = new StringTokenizer(l, ",");
        final List<String> rc = new ArrayList<>();
        while (st.hasMoreTokens()) {
            rc.add(st.nextToken());
        }
        LOG.debug("result of tokens: {}", rc);
        return rc;
    }
}
