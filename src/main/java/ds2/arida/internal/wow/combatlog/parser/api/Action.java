/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package ds2.arida.internal.wow.combatlog.parser.api;

/**
 * The action is a spell, a weapon swing, or any "action" that the originator
 * did to a target.
 * 
 * @author dstrauss
 * @version 1.0
 */
public interface Action extends DamageActionValues, AuraActionValues, Spell {
    /**
     * Returns the name of the action, as found in the log files. This can be a
     * spell name, a buff name etc.
     * 
     * @return the action name
     */
    String getName();
    
    /**
     * Returns the amount of damage, or healing etc.
     * 
     * @return the amount, or -1 if not applicable
     */
    int getAmount();
    
}
