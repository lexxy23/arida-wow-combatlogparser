/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package ds2.arida.internal.wow.combatlog.parser.impl;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ds2.arida.internal.wow.combatlog.parser.api.GuidParser;
import ds2.arida.internal.wow.combatlog.parser.api.Unit;
import ds2.arida.internal.wow.combatlog.parser.api.UnitType;
import ds2.arida.internal.wow.combatlog.parser.impl.dto.UnitDto;
import ds2.core.api.BitSupport;
import ds2.core.api.svc.HexCodec;

/**
 * The initial implementation.
 * 
 * @author dstrauss
 * @version 1.0
 */
public class GuidParserImpl implements GuidParser {
    /**
     * The unit id of a vehicle.
     */
    private static final int UNITID_VEHICLE = 5;
    /**
     * The unit id of a pet.
     */
    private static final int UNITID_PERMPET = 4;
    /**
     * The unit id of an npc.
     */
    private static final int UNITID_NPC = 3;
    /**
     * The unit id of a world object.
     */
    private static final int UNITID_WORLDOBJECT = 1;
    /**
     * The unit id of a player.
     */
    private static final int UNITID_PLAYER = 0;
    /**
     * A logger.
     */
    private static final Logger LOG = LoggerFactory
        .getLogger(GuidParserImpl.class);
    /**
     * The hex codec.
     */
    @Inject
    private HexCodec hx;
    /**
     * For binary operations.
     */
    @Inject
    private BitSupport bit;
    
    /**
     * Inits the impl.
     */
    public GuidParserImpl() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * Handles the data for an npc.
     * 
     * @param u
     *            the dto to fill
     * @param b
     *            the known bytes
     */
    private void handleNpc(final UnitDto u, final byte[] b) {
        u.setType(UnitType.NPC);
        final byte dLast = (byte) (b[1] & 0x0F);
        final int npcId = bit.createInt(dLast, b[2], b[3]);
        // final int npcId = bit.createInt(b[3], b[4]);
        u.setUnitId(npcId);
        final int spawnCounter = bit.createInt(b[5], b[6], b[7]);
        u.setSpawnId(spawnCounter);
    }
    
    /**
     * Handles the data parsing for a pet.
     * 
     * @param u
     *            the pet unit
     * @param b
     *            the received bytes
     */
    private void handlePet(final UnitDto u, final byte[] b) {
        u.setType(UnitType.PERMPET);
        final byte dLast = (byte) (b[1] & 0x0F);
        final int uId = bit.createInt(dLast, b[2], b[3], b[4]);
        u.setUnitId(uId);
    }
    
    /**
     * Handles the data parsing for a player.
     * 
     * @param u
     *            the unit dto to fill
     * @param b
     *            the parsed bytes
     */
    private void handlePlayer(final UnitDto u, final byte[] b) {
        u.setType(UnitType.PLAYER);
        final byte dLast = (byte) (b[4] & 0x0F);
        final int uId = bit.createInt(dLast, b[5], b[6], b[7]);
        u.setUnitId(uId);
    }
    
    /**
     * Handler for vehicles.
     * 
     * @param u
     *            the unit dto
     * @param b
     *            the bytes to scan
     */
    private void handleVehicle(final UnitDto u, final byte[] b) {
        u.setType(UnitType.VEHICLE);
        final byte dLast = (byte) (b[1] & 0x0F);
        final int uId = bit.createInt(dLast, b[2], b[3]);
        u.setUnitId(uId);
        final int spawnCounter = bit.createInt(b[4], b[5], b[6], b[7]);
        u.setSpawnId(spawnCounter);
    }
    
    /**
     * The handler for world objects.
     * 
     * @param u
     *            the unit dto
     * @param b
     *            the bytes
     */
    private void handleWorldObject(final UnitDto u, final byte[] b) {
        u.setType(UnitType.WORLDOBJECT);
        final byte dLast = (byte) (b[1] & 0x0F);
        final int uId = bit.createInt(dLast, b[2], b[3], b[4]);
        u.setUnitId(uId);
    }
    
    @Override
    public final Unit parse(final String guid) {
        LOG.debug("Entering with guid={}", guid);
        if (guid == null) {
            LOG.debug("No guid was given to parse!");
            return null;
        }
        final byte[] bytes = hx.decode(guid.toLowerCase().toCharArray());
        // should be 64 bit -> long
        if (bytes == null || bytes.length != 8) {
            LOG.warn("Strange count of bytes to parse guid!");
            return null;
        }
        final long guidL = bit.createLong(bytes);
        Unit rc = null;
        try {
            final UnitDto u = new UnitDto();
            u.setGuid(guidL);
            u.setBgIdentifier(Integer.valueOf(bytes[0]));
            final int unitTypeId = (bytes[1] & 0x70) >>> 4;
            switch (unitTypeId) {
                case UNITID_PLAYER:
                    handlePlayer(u, bytes);
                    break;
                case UNITID_WORLDOBJECT:
                    handleWorldObject(u, bytes);
                    break;
                case UNITID_NPC:
                    handleNpc(u, bytes);
                    break;
                case UNITID_PERMPET:
                    handlePet(u, bytes);
                    break;
                case UNITID_VEHICLE:
                    handleVehicle(u, bytes);
                    break;
                default:
                    LOG.debug("Cannot parse unitTypeId {}, ignoring.",
                        unitTypeId);
                    break;
            }
            rc = u;
        } catch (final NumberFormatException e) {
            LOG.debug("Error parsing guid!", e);
        }
        LOG.debug("rc will be {}", rc);
        return rc;
    }
}
