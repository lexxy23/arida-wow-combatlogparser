/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ds2.arida.internal.wow.combatlog.parser.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ds2.arida.internal.wow.combatlog.parser.api.EventTimeParser;
import ds2.arida.internal.wow.combatlog.parser.api.ParserSettings;

/**
 * The implementation for the event time parser.
 * 
 * @author dstrauss
 * @version 1.0
 */
public class EventTimeParserImpl implements EventTimeParser {
    /**
     * The pattern for the internal timestamp.
     */
    private static final String INTERNAL_DATEFORMAT = "yyyyMM/dd HH:mm:ss.SSS";
    /**
     * A logger.
     */
    private static final Logger LOG = LoggerFactory
        .getLogger(EventTimeParserImpl.class);
    /**
     * The parser settings.
     */
    @Inject
    private ParserSettings setts;
    
    /**
     * Inits the time parser.
     */
    public EventTimeParserImpl() {
        // TODO Auto-generated constructor stub
    }
    
    /*
     * (non-Javadoc)
     * @see ds2.arida.internal.wow.combatlog.parser.api.EventTimeParser#
     * parseTimeFromLogline(java.lang.String)
     */
    @Override
    public final Date parseTimeFromLogline(final String logline) {
        LOG.debug("Given logLine is: {}", logline);
        if (logline == null || logline.length() < 18) {
            LOG.debug("No logline given to parse!");
            return null;
        }
        final String dateStr = logline.substring(0, 18);
        Date rc = null;
        final String foundDateStr = setts.getInitialYear() + dateStr;
        final SimpleDateFormat sdf = new SimpleDateFormat(INTERNAL_DATEFORMAT);
        try {
            rc = sdf.parse(foundDateStr);
        } catch (final ParseException e) {
            LOG.debug("Error when parsing the given found date!", e);
        }
        LOG.debug("rc is {}", rc);
        return rc;
    }
}
