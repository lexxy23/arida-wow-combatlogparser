/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package ds2.arida.internal.wow.combatlog.parser.impl;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ds2.arida.internal.wow.combatlog.parser.api.UnitFlag;
import ds2.arida.internal.wow.combatlog.parser.api.UnitFlagController;
import ds2.arida.internal.wow.combatlog.parser.api.UnitFlagControllerAffiliation;
import ds2.arida.internal.wow.combatlog.parser.api.UnitFlagParser;
import ds2.arida.internal.wow.combatlog.parser.api.UnitFlagReaction;
import ds2.arida.internal.wow.combatlog.parser.api.UnitFlagType;
import ds2.arida.internal.wow.combatlog.parser.impl.dto.UnitFlagDto;
import ds2.core.api.BitSupport;
import ds2.core.api.svc.HexCodec;

/**
 * Basic detection for unit flags.
 * 
 * @author dstrauss
 * @version 1.0
 */
public class UnitFlagParserImpl implements UnitFlagParser {
    /**
     * The mask for the affiliate data.
     */
    private static final int UFMASK_AFL = 0xf;
    /**
     * The mask for the reaction data.
     */
    private static final int UFMASK_REACTION = 0xf0;
    /**
     * The mask for the controller.
     */
    private static final int UFMASK_CONTROLLER = 0x300;
    /**
     * The mask for the type.
     */
    private static final int UFMASK_TYPE = 0xfc00;
    /**
     * The 8.
     */
    private static final int EIGHT = 8;
    /**
     * The 4.
     */
    private static final int FOUR = 4;
    /**
     * A logger.
     */
    private static final Logger LOG = LoggerFactory
        .getLogger(UnitFlagParserImpl.class);
    /**
     * The hex codec.
     */
    @Inject
    private HexCodec hx;
    /**
     * The bit support.
     */
    @Inject
    private BitSupport bit;
    
    /**
     * INits the bean.
     */
    public UnitFlagParserImpl() {
        // nothing special to do
    }
    
    @Override
    public final UnitFlag parseUnitFlag(final int flag) {
        LOG.debug("Got flag to parse: {}", Integer.valueOf(flag));
        final UnitFlagDto rc = new UnitFlagDto();
        final int typeData = (flag & UFMASK_TYPE) >>> EIGHT;
        final int controllerData = (flag & UFMASK_CONTROLLER) >>> EIGHT;
        final int reactionData = (flag & UFMASK_REACTION) >> FOUR;
        final int aflData = flag & UFMASK_AFL;
        rc.setType(getType(typeData));
        rc.setController(getController(controllerData));
        rc.setReaction(getReaction(reactionData));
        rc.setControllerAffiliation(getAffiliation(aflData));
        LOG.debug("RC will be {}", rc);
        return rc;
    }
    
    /**
     * Guesses the affiliation.
     * 
     * @param aflData
     *            the affiliation flag
     * @return the affiliation
     */
    private UnitFlagControllerAffiliation getAffiliation(final int aflData) {
        UnitFlagControllerAffiliation rc = null;
        switch (aflData) {
            case EIGHT:
                rc = UnitFlagControllerAffiliation.OUTSIDER;
                break;
            case FOUR:
                rc = UnitFlagControllerAffiliation.RAID;
                break;
            case 2:
                rc = UnitFlagControllerAffiliation.PARTY;
                break;
            case 1:
                rc = UnitFlagControllerAffiliation.MINE;
                break;
            default:
                LOG.warn("Unknown aflData: {}", Integer.valueOf(aflData));
                break;
        }
        return rc;
    }
    
    /**
     * Guesses the reaction data.
     * 
     * @param reactionData
     *            the reaction flag.
     * @return the reaction
     */
    private UnitFlagReaction getReaction(final int reactionData) {
        UnitFlagReaction rc = null;
        switch (reactionData) {
            case FOUR:
                rc = UnitFlagReaction.HOSTILE;
                break;
            case 2:
                rc = UnitFlagReaction.NEUTRAL;
                break;
            case 1:
                rc = UnitFlagReaction.FRIENDLY;
                break;
            default:
                LOG.warn("Unknown reactionData: {}",
                    Integer.valueOf(reactionData));
                break;
        }
        return rc;
    }
    
    /**
     * Guesses the controller.
     * 
     * @param controllerData
     *            the controller data flag
     * @return the controller
     */
    private UnitFlagController getController(final int controllerData) {
        UnitFlagController rc = null;
        switch (controllerData) {
            case 1:
                rc = UnitFlagController.Player;
                break;
            case 2:
                rc = UnitFlagController.NPC;
                break;
            default:
                LOG.warn("Unknown controllerData: {}",
                    Integer.valueOf(controllerData));
                break;
        }
        return rc;
    }
    
    /**
     * Guesses the type from the given flag.
     * 
     * @param typeData
     *            the type data flag
     * @return the possible type, or null if unknown
     */
    private UnitFlagType getType(final int typeData) {
        UnitFlagType rc = null;
        switch (typeData) {
            case 64:
                rc = UnitFlagType.Object;
                break;
            case 32:
                rc = UnitFlagType.Guardian;
                break;
            case 16:
                rc = UnitFlagType.Pet;
                break;
            case EIGHT:
                rc = UnitFlagType.NPC;
                break;
            case FOUR:
                rc = UnitFlagType.Player;
                break;
            default:
                LOG.warn("Unknown typeData: {}", Integer.valueOf(typeData));
                break;
        }
        return rc;
    }
    
    @Override
    public final UnitFlag parseUnitFlagFromHexString(final String string) {
        final byte[] b = hx.decode(string.toCharArray());
        final int flag = bit.createInt(b);
        return this.parseUnitFlag(flag);
    }
    
}
