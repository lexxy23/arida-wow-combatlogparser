/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ds2.arida.internal.wow.combatlog.parser.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ds2.arida.internal.wow.combatlog.parser.api.Action;
import ds2.arida.internal.wow.combatlog.parser.api.ActionParser;
import ds2.arida.internal.wow.combatlog.parser.api.AuraType;
import ds2.arida.internal.wow.combatlog.parser.api.EventTypeEnum;
import ds2.arida.internal.wow.combatlog.parser.api.SimpleTypesParser;
import ds2.arida.internal.wow.combatlog.parser.impl.dto.ActionDto;
import ds2.core.api.BitSupport;
import ds2.core.api.svc.HexCodec;

/**
 * The parser for the actions.
 * 
 * @author dstrauss
 * @version 1.0
 */
public class ActionParserImpl implements ActionParser {
    /**
     * A logger.
     */
    private static final Logger LOG = LoggerFactory
        .getLogger(ActionParserImpl.class);
    /**
     * A simple parser.
     */
    @Inject
    private SimpleTypesParser parser;
    /**
     * The hex codec.
     */
    @Inject
    private HexCodec hx;
    /**
     * The bit support.
     */
    @Inject
    private BitSupport bit;
    
    /**
     * Inits the impl.
     */
    public ActionParserImpl() {
        // nothing special to do.
    }
    
    @Override
    public final Action parseAction(final EventTypeEnum type,
        final List<String> tokens) {
        LOG.debug("Got data: {}", new Object[] { type, tokens });
        final List<String> myTokenList = new ArrayList<>(tokens.size());
        myTokenList.addAll(tokens);
        ActionDto rc = new ActionDto();
        switch (type.getPrefix()) {
            case SWING:
                break;
            case SPELL:
            case RANGE:
            case SPELL_BUILDING:
            case SPELL_PERIODIC:
                rc.setSpellId(parser.toInt(myTokenList.get(0), -1));
                rc.setName(parser.parseString(myTokenList.get(1)));
                rc.setSpellSchoolId(bit.createInt(hx.decode(myTokenList.get(2)
                    .toCharArray())));
                shift(myTokenList, 3);
                break;
            default:
                LOG.warn("Not yet implemented: {}", type.getPrefix());
                break;
        }
        switch (type.getSuffix()) {
            case AURA_REMOVED:
                rc.setAuraType(AuraType.valueOf(myTokenList.get(0)));
                break;
            case DAMAGE:
                // 28941,-1,1,0,0,0,1,nil,nil
                rc.setAmount(parser.toInt(myTokenList.get(0), -1));
                rc.setOverKillAmount(parser.toInt(myTokenList.get(1), -1));
                rc.setSpellSchoolId(parser.toInt(myTokenList.get(2), 0));
                rc.setResistedAmount(parser.toInt(myTokenList.get(3), 0));
                rc.setBlockedAmount(parser.toInt(myTokenList.get(4), 0));
                rc.setAbsorbedAmount(parser.toInt(myTokenList.get(5), 0));
                rc.setCritical(parser.toBoolean(myTokenList.get(6), false));
                rc.setGlancing(parser.toBoolean(myTokenList.get(7), false));
                rc.setCrushing(parser.toBoolean(myTokenList.get(8), false));
                break;
            default:
                LOG.warn("Unknown type: {}", type);
                rc = null;
                break;
        }
        LOG.debug("returning action: {}", rc);
        return rc;
    }
    
    /**
     * Removes the specific count of element 0 from the given list.
     * 
     * @param myTokenList
     *            the list to shift elements
     * @param count
     *            the count of element 0 to remove
     */
    private static void shift(final List<String> myTokenList, final int count) {
        for (int i = 0; i < count; i++) {
            myTokenList.remove(0);
        }
    }
}
