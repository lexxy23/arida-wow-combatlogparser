/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ds2.arida.internal.wow.combatlog.parser.api;

/**
 * The known guid types.
 * 
 * @author dstrauss
 * @version 1.0
 */
public enum UnitType {
    /**
     * A player.
     */
    PLAYER(0),
    /**
     * A world object.
     */
    WORLDOBJECT(1),
    /**
     * An NPC. May be a boss, a merchant.
     */
    NPC(3),
    /**
     * A permanent pet.
     */
    PERMPET(4),
    /**
     * A vehicle.
     */
    VEHICLE(5);
    /**
     * The id, as found in the combat log guid.
     */
    private int id;
    
    /**
     * Inits the enum value.
     * 
     * @param i
     *            the id
     */
    private UnitType(final int i) {
        id = i;
    }
    
    /**
     * Returns the id of this type.
     * 
     * @return the id
     */
    public int getId() {
        return id;
    }
}
