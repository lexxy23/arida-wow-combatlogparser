/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ds2.arida.internal.wow.combatlog.parser.impl.dto;

import java.util.Date;

import ds2.arida.internal.wow.combatlog.parser.api.Action;
import ds2.arida.internal.wow.combatlog.parser.api.Event;
import ds2.arida.internal.wow.combatlog.parser.api.EventTypeEnum;
import ds2.arida.internal.wow.combatlog.parser.api.Unit;

/**
 * A simple combat line.
 * 
 * @author dstrauss
 * @version 1.0
 */
public class CombatLogLine implements Event {
    /**
     * The svuid.
     */
    private static final long serialVersionUID = 44015771069693463L;
    /**
     * The time of the event.
     */
    private Date timestamp;
    /**
     * The action.
     */
    private Action action;
    /**
     * The originator.
     */
    private Unit originator;
    /**
     * The target.
     */
    private Unit target;
    /**
     * The event type.
     */
    private EventTypeEnum eventType;
    
    /**
     * Inits the line.
     */
    public CombatLogLine() {
        // nothing special to do
    }
    
    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof CombatLogLine)) {
            return false;
        }
        final CombatLogLine other = (CombatLogLine) obj;
        if (timestamp == null) {
            if (other.timestamp != null) {
                return false;
            }
        } else if (!timestamp.equals(other.timestamp)) {
            return false;
        }
        return true;
    }
    
    @Override
    public final Date getTimestamp() {
        return timestamp;
    }
    
    @Override
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result =
            prime * result + (timestamp == null ? 0 : timestamp.hashCode());
        return result;
    }
    
    /**
     * Sets the time of the event.
     * 
     * @param time
     *            the timestamp to set
     */
    public final void setTimestamp(final Date time) {
        this.timestamp = time;
    }
    
    @Override
    public final String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("CombatLogLine [timestamp=");
        builder.append(timestamp);
        builder.append(", action=");
        builder.append(action);
        builder.append(", originator=");
        builder.append(originator);
        builder.append(", target=");
        builder.append(target);
        builder.append(", eventType=");
        builder.append(eventType);
        builder.append("]");
        return builder.toString();
    }
    
    @Override
    public final Unit getOriginator() {
        return originator;
    }
    
    @Override
    public final Unit getTarget() {
        return target;
    }
    
    @Override
    public final Action getAction() {
        return action;
    }
    
    @Override
    public final int compareTo(final Event o) {
        if (o == null) {
            return 1;
        }
        final Date thisTime = timestamp;
        final Date otherDate = o.getTimestamp();
        return thisTime.compareTo(otherDate);
    }
    
    /**
     * Sets the action that was performed.
     * 
     * @param a
     *            the action to set
     */
    public final void setAction(final Action a) {
        this.action = a;
    }
    
    /**
     * Sets the originator. This is the one that started an action.
     * 
     * @param o
     *            the originator to set
     */
    public final void setOriginator(final Unit o) {
        this.originator = o;
    }
    
    /**
     * Sets the target of an action.
     * 
     * @param t
     *            the target to set
     */
    public final void setTarget(final Unit t) {
        this.target = t;
    }
    
    @Override
    public final EventTypeEnum getEventType() {
        return eventType;
    }
    
    /**
     * Sets the event type.
     * 
     * @param eType
     *            the eventType to set
     */
    public final void setEventType(final EventTypeEnum eType) {
        this.eventType = eType;
    }
    
}
