/*
 * WoW Combatlog Parser - The combatlog parser for the arida RM
 * Copyright (C) 2011  Dirk Strauss
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ds2.arida.internal.wow.combatlog.parser.api;

/**
 * Possible event suffixes.
 * 
 * @author dstrauss
 * @version 1.0
 */
public enum EventSuffix {
    AURA_APPLIED, AURA_APPLIED_DOSE, AURA_BROKEN, AURA_BROKEN_SPELL,
    AURA_REFRESH,
    /**
     * An aura has been removed.
     */
    AURA_REMOVED, AURA_REMOVED_DOSE, CAST_FAILED, CAST_START, CAST_SUCCESS,
    CREATE,
    /**
     * Damage has been done.
     */
    DAMAGE, DISPEL, DISPEL_FAILED, DRAIN, DURABILITY_DAMAGE,
    DURABILITY_DAMAGE_ALL, ENERGIZE, EXTRA_ATTACKS, HEAL, INSTAKILL, INTERRUPT,
    LEECH, MISSED, RESURRECT, STOLEN, SUMMON;
}
