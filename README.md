Arida WoW Combatlog Parser
==========================

a JEE6 library/module to parse the WoW combatlog entries.

# License
Lesser General Public License v3

# How to build

* use Apache Maven 3.0.4, or better
* use Java 7

Command line to execute:

	mvn clean install -Pno-repo-man
